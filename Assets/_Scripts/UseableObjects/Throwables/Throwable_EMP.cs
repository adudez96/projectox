﻿using UnityEngine;
using System.Collections;

public class Throwable_EMP : MonoBehaviour, Throwable {

	Vector3 throwPos;

	float sonarTimer;
	float fuseTimer = 5f;

	float blastRadius = 15f;

	GameObject blast = null;

	GameObject[] enemies;

	public AudioSource sonarGrenadeAudio;
	public AudioClip sonarGrenadeClip;

	void Awake()
	{
		sonarTimer = 0; 
		blast = GameObject.CreatePrimitive (PrimitiveType.Sphere);
		blast.GetComponent<Collider> ().isTrigger = true;
		blast.transform.localScale = new Vector3 (0f, 0f, 0f);

		enemies = GameObject.FindGameObjectsWithTag ("Enemy");
	}

	// Update is called once per frame
	void Update () {
		sonarTimer += Time.deltaTime;

		if (sonarTimer >= fuseTimer - 0.5f) {
			blast.transform.position = this.transform.position;
			blast.transform.localScale += new Vector3(blastRadius * Time.deltaTime * 2, blastRadius*Time.deltaTime * 2, blastRadius*Time.deltaTime * 2);
		}

		if (sonarTimer >= fuseTimer) {
			// Damage enemies... TODO

			foreach (GameObject g in enemies) {
				if ((g.transform.position - this.transform.position).magnitude <= blastRadius) {
					g.GetComponent<Enemy> ().ModifyRange (1.0f, 5f);
				}
			}

			Destroy (this.gameObject);
			Destroy (blast);
		}
	}


	/******************************************************************************************************************
	 ********************************************** THROWABLE *********************************************************
	 *****************************************************************************************************************/

	public void Throw (Vector3 pos, float force) {
		//
	}
}
