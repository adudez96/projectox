﻿using UnityEngine;
using System.Collections;

public interface Throwable 
{
	/**
	 * Throw the throwable object towards 'pos' with 'force' force
	 */
	void Throw (Vector3 pos, float force);
}
