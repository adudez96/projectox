﻿using UnityEngine;
using System.Collections;

public class Collectible : MonoBehaviour {

	public int type;

	// Use this for initialization
	void Awake () {
	
	}
	
	// Update is called once per frame
	void OnTriggerEnter (Collider other) {
		if (other.CompareTag ("Player")) {
			if (other.gameObject.GetComponent<ObjectUser> ().Collect(type)) {
				Destroy(this.gameObject);
			}
		}
	}
}
