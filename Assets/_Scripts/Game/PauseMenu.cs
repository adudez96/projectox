﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour 
{
	//CLASS INSTANCES

	//PUBLIC VARIABLES
	public static bool isPaused;


	//GAMEOBJECTS AND COMPONENTS
	GameObject pMenu;


	//PRIVATE/LOCAL VARIABLES


	// Use this for initialization
	void Awake () 
	{
		pMenu = GameObject.FindGameObjectWithTag ("PauseMenu");
		isPaused = false;
	}
	
	// Update is called once per frame
	void Update () 
	{
		ActivatePause ();
		CallPause ();
	
	}

	void ActivatePause ()
	{
		if (Input.GetKeyDown (KeyCode.Escape) || Input.GetKeyDown (KeyCode.P)) 
		{
			isPaused = !isPaused;
		}
	}

	void CallPause()
	{
		if (isPaused) 
		{
			pMenu.SetActive (true);
			Time.timeScale = 0f;
		} 

		else 
		{
			Resume ();
		}

	}

	public void Resume()
	{
		isPaused = false;
		pMenu.SetActive (false);
		Time.timeScale = 1f;
	}

	public void NewGame()
	{
		SceneManager.LoadScene(1);
	}

	public void LoadGame()
	{
		//To be coded once first level is done
	}

	public void QuitGame()
	{
		Application.Quit ();
	}

}
