﻿using UnityEngine;
using System.Collections;

public class TeamEnemyParent : MonoBehaviour, Enemy {
	
	// INSTANCE VARIABLES
	private EnemyState state;
	public Vector3 playerLKP;
	private float FOVAngle;
	private float range;
	private float speed;

	// PUBLICLY EDITABLE VARIABLES
	public float defaultFOVAngle;
	public float defaultRange;
	public float defaultSpeed;
	public int attackDamage;


	// OBJECT COMPONENTS


	// EXTERNAL OBJECTS/COMPONENTS
	public GameObject[] gchildren;
	private TeamEnemyChild[] childrenObj;
	GameObject player;

	// PRIVATES AND TIMERS
	float searchTimer;


	// **********************************************************************************************
	// ************************************** MONOBEHAVIOUR *****************************************
	// **********************************************************************************************

	// Use this for initialization
	void Awake () {
		// get the children
		childrenObj = new TeamEnemyChild[gchildren.Length];
		int i = 0;
		foreach (GameObject g in gchildren) {
			childrenObj [i] = g.GetComponent<TeamEnemyChild> ();
			i++;
		}

		state = EnemyState.IDLE;

		FOVAngle = defaultFOVAngle;
		range = defaultRange;
		speed = defaultSpeed;

		player = GameObject.FindGameObjectWithTag ("Player");

		searchTimer = 0f;
	}
	
	// Update is called once per frame
	void Update () {
		print ("TEAM ENEMY's STATE: " + state);

		if (state == EnemyState.ALERT) {

			playerLKP = player.transform.position;
			searchTimer = 5f;
		} else if (state == EnemyState.SEARCHING) {
			searchTimer -= Time.deltaTime;

			if (searchTimer > 0f) {
			} else {
				state = EnemyState.IDLE;
			}
		} else if (state == EnemyState.IDLE) 
		{
			// do nothing... children will handle that themselves
		}
	}

	// **********************************************************************************************
	// ****************************************** ENEMY *********************************************
	// **********************************************************************************************

	/**
	 * Return the current state of the enemy
	 */
	public EnemyState GetState () {
		return state;
	}

	/**
	 * Return the current state of the enemy
	 */
	public float GetRange () {
		return range;
	}

	/**
	 * Return the current speed of the enemy
	 */
	public float GetSpeed () {
		return speed;
	}

	/**
	 * Return the current FOV angle of the enemy
	 */
	public float GetFOVAngle () {
		return FOVAngle;
	}

	/**
	 * Given a state to change to, change the enemy's state
	 */
	public void ChangeState (EnemyState s)
	{
		state = s;
	}

	/**
	 * Sight the transform
	 * Useful for alerting enemy in sonar
	 */
	public void SightMe (Transform t) {
		state = EnemyState.SEARCHING;
		playerLKP = t.position;
		searchTimer = 5f;
	}

	/**
	 * Sight the player
	 * Useful for alerting enemy when in FOV
	 */
	public void SightPlayer () {
		state = EnemyState.ALERT;
	}

	/**
	 * Lose sight of player
	 * Useful for starting search phase, returning to idle
	 */
	public void LostSightPlayer () {
		if (state == EnemyState.ALERT) {
			state = EnemyState.SEARCHING;
			searchTimer = 5f;
		}
	}

	/**
	 * Given a float value, multiply current speed by that value for specified time
	 */
	public void ModifySpeed (float mod, float time) {

	}

	/**
	 * Given a float value, multiply current range by that value for specified time
	 */
	public void ModifyRange (float mod, float time) {

	}

	/**
	 * Given a float value, multiply current FOV angle by that value for specified time
	 */
	public void ModifyFOV (float mod, float time) {

	}


	// **********************************************************************************************
	// ***************************************** PRIVATES *******************************************
	// **********************************************************************************************

}
