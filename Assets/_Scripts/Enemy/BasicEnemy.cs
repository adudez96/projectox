﻿using UnityEngine;
using System.Collections;

using UnityEngine.UI;

public class BasicEnemy : MonoBehaviour, Enemy {

	// INSTANCE VARIABLES
	private EnemyState state;
	private Vector3 playerLKP;
	private float FOVAngle;
	private float range;
	private float speed;
	private int currentPatrolPt;

	// PUBLICLY EDITABLE VARIABLES
	public float defaultFOVAngle;
	public float defaultRange;
	public float defaultSpeed;
	public float rotationSpeed;
	public int attackDamage;
	public float defaultSearchTime;

	public Transform[] patrolPoints;

	// OBJECT COMPONENTS
	NavMeshAgent nav;

	Quaternion lookRotation;
	Vector3 lookDirection;

	// EXTERNAL OBJECTS/COMPONENTS
	GameObject player;
	PlayerHealth pHealth;

	GameObject sIconIdle;
	GameObject sIconStunned;
	GameObject sIconSearching;
	GameObject sIconAlert;

	// PRIVATES AND TIMERS
	float patrolPointTimer;
	private float searchTimer;
	Vector3 currentDest;
	private float speedMod;
	private float speedModTimer;
	private float rangeMod;
	private float rangeModTimer;

	// TEST STUFF
	public Text timerText;
	public Text enemyStateText;

	// **********************************************************************************************
	// ************************************** MONOBEHAVIOUR *****************************************
	// **********************************************************************************************

	// Use this for initialization
	void Awake () {
		player = GameObject.FindGameObjectWithTag ("Player");
		pHealth = player.GetComponent<PlayerHealth> ();
		nav = GetComponent<NavMeshAgent> ();

		state = EnemyState.IDLE;
		FOVAngle = defaultFOVAngle;
		range = defaultRange;
		speed = defaultSpeed;
		currentPatrolPt = 0;

		nav.speed = speed;

		speedMod = 0;
		rangeMod = 0;
		speedModTimer = 0;
		rangeModTimer = 0;

		//Enemy status icon assignment and initialization
		sIconIdle = GameObject.FindGameObjectWithTag("Icon_Idle");
		sIconStunned = GameObject.FindGameObjectWithTag("Icon_Stunned");
		sIconSearching = GameObject.FindGameObjectWithTag("Icon_Searching");
		sIconAlert = GameObject.FindGameObjectWithTag("Icon_Alert");

		sIconIdle.SetActive (false);
		sIconStunned.SetActive (false);
		sIconSearching.SetActive (false);
		sIconAlert.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
		//print ("ENEMY's STATE: " + state);

		if (timerText != null)
			timerText.text = speedModTimer.ToString();
		if (enemyStateText != null)
			enemyStateText.text = speed.ToString ();
		
		if (state == EnemyState.ALERT) {

			//Change status icon
			sIconIdle.SetActive(false);
			sIconStunned.SetActive (false);
			sIconSearching.SetActive (false);
			sIconAlert.SetActive (true);

			playerLKP = player.transform.position;
			currentDest = playerLKP;
			searchTimer = 5f;

			if (InFOV(player.transform) && ((player.transform.position - transform.position).magnitude <= 0.5)) {
				AttackPlayer(pHealth);
			}
		} else if (state == EnemyState.SEARCHING) {
			if ((transform.position - currentDest).magnitude <= 1f) {
				searchTimer -= Time.deltaTime;
			}

			if (searchTimer > 0f) {
				currentDest = playerLKP;
			} else {
				state = EnemyState.IDLE;
			}
		} else if (state == EnemyState.IDLE) 
		{
			//Change status icon
			sIconIdle.SetActive(true);
			sIconStunned.SetActive(false);
			sIconSearching.SetActive (false);
			sIconAlert.SetActive (false);
			currentDest = Patrol ();
		}
			
		if (speedModTimer > 0) {
			speedModTimer -= Time.deltaTime;
			speed = defaultSpeed - (defaultSpeed * speedMod);
			if (speedModTimer <= 0) {
				SightMe (player.transform);
				speed = defaultSpeed;
				print ("in here");
			}
		}

		if (rangeModTimer > 0) {
			rangeModTimer -= Time.deltaTime;
			range = defaultRange  - (range * rangeMod);
		} else {
			range = defaultRange;
		}

		nav.SetDestination (currentDest);
		nav.speed = speed;
	}


	// **********************************************************************************************
	// ****************************************** ENEMY *********************************************
	// **********************************************************************************************

	/**
	 * Return the current state of the enemy
	 */
	public EnemyState GetState () {
		return state;
	}

	/**
	 * Return the current range of the enemy
	 */
	public float GetRange () {
		return range;
	}

	/**
	 * Return the current speed of the enemy
	 */
	public float GetSpeed () {
		return speed;
	}

	/**
	 * Return the current FOV angle of the enemy
	 */
	public float GetFOVAngle () {
		return FOVAngle;
	}

	/**
	 * Given a state to change to, change the enemy's state
	 */
	public void ChangeState (EnemyState s)
	{
		state = s;
	}

	/**
	 * Sight the transform
	 * Useful for alerting enemy in sonar
	 */
	public void SightMe (Transform t) {
		state = EnemyState.SEARCHING;
		playerLKP = t.position;
		searchTimer = 5f;
	}

	/**
	 * Sight the player
	 * Useful for alerting enemy when in FOV
	 */
	public void SightPlayer () {
		state = EnemyState.ALERT;
	}

	/**
	 * Lose sight of player
	 * Useful for starting search phase, returning to idle
	 */
	public void LostSightPlayer () {
		if (state == EnemyState.ALERT) {
			state = EnemyState.SEARCHING;
			searchTimer = 5f;
		}
	}
		

	/**
	 * Given a float value, multiply current speed by that value for specified time
	 **/
	public void ModifySpeed (float mod, float time)
	{
		speedMod = mod;
		speedModTimer = time;
	}

	/**
	 * Given a float value, multiply current range by that value for specified time
	 */
	public void ModifyRange (float mod, float time)
	{
		rangeMod = mod;
		rangeModTimer = time;
	}

	/**
	 * Given a float value, multiply current FOV angle by that value for specified time
	 */
	public void ModifyFOV (float mod, float time)
	{

	}
		

	// **********************************************************************************************
	// ***************************************** PRIVATES *******************************************
	// **********************************************************************************************

	/**
	 * Given a player GameObject to attack, begin attacking /damaging that player
	 */
	private void AttackPlayer (PlayerHealth playerHealth)
	{
		playerHealth.TakeDamage(attackDamage);
	}


	/**
	 * If enemy is in the 'IDLE' state, this method will be used
	 */
	Vector3 Patrol()
	{
		if (patrolPoints.Length == 0) {
			return transform.position;
		}

		if (transform.position == nav.destination) 
		{
			int nextPatrolPoint = currentPatrolPt+1;

			if(nextPatrolPoint > (patrolPoints.Length-1))
			{
				nextPatrolPoint = 0;
			}


			if (patrolPointTimer <= 0.4f) 
			{
				lookDirection = ((patrolPoints[nextPatrolPoint].position-transform.position).normalized);
				lookRotation = Quaternion.LookRotation(lookDirection);
				transform.rotation = Quaternion.Slerp (transform.rotation, lookRotation, rotationSpeed*Time.deltaTime);
			}
			if (patrolPointTimer <= 0)
			{ 

				currentPatrolPt++;
				patrolPointTimer = 3f;
			}

			patrolPointTimer -= Time.deltaTime;
		}

		if (currentPatrolPt > patrolPoints.Length - 1) 
		{
			currentPatrolPt = 0;
		}

		//Change status icon
		sIconIdle.SetActive(true);

		return patrolPoints [currentPatrolPt].position;
	}

	/**
	 * Given a Transform, check if that transform is within the FOV
	 */
	private bool InFOV (Transform other) {
		float angle = Vector3.Angle(transform.forward, other.position - transform.position);
		return angle <= 0.5f * FOVAngle;
	}
}
