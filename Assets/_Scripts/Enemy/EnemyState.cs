﻿using UnityEngine;
using System.Collections;

public enum EnemyState {
	IDLE, 
	SEARCHING, 
	ALERT
}
