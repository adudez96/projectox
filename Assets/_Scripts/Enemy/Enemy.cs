﻿using UnityEngine;
using System.Collections;

public interface Enemy {

	/**
	 * Return the current state of the enemy
	 */
	EnemyState GetState ();

	/**
	 * Return the current state of the enemy
	 */
	float GetRange ();

	/**
	 * Return the current speed of the enemy
	 */
	float GetSpeed ();

	/**
	 * Return the current FOV angle of the enemy
	 */
	float GetFOVAngle ();

	/**
	 * Given a state to change to, change the enemy's state
	 */
	void ChangeState (EnemyState state);

	/**
	 * Sight the transform
	 * Useful for alerting enemy in sonar
	 */
	void SightMe (Transform t);

	/**
	 * Sight the player
	 * Useful for alerting enemy when in FOV
	 */
	void SightPlayer ();

	/**
	 * Lose sight of player
	 * Useful for starting search phase, returning to idle
	 */
	void LostSightPlayer ();

	/**
	 * Given a float value, multiply current speed by that value for specified time
	 */
	void ModifySpeed (float mod, float time);

	/**
	 * Given a float value, multiply current range by that value for specified time
	 */
	void ModifyRange (float mod, float time);

	/**
	 * Given a float value, multiply current FOV angle by that value for specified time
	 */
	void ModifyFOV (float mod, float time);
}
