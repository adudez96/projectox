﻿using UnityEngine;
using System.Collections;

public class EnemySight : MonoBehaviour {

	// INSTANCE VARIABLES

	// PUBLICLY EDITABLE VARIABLES
	public bool isXRay;

	// OBJECT COMPONENTS
	SphereCollider rangeSphereCol;

	// EXTERNAL OBJECTS/COMPONENTS
	Transform player;
	Enemy parentEnemy;

	// PRIVATES AND TIMERS
	float initialBufferTimer;


	// **********************************************************************************************
	// ************************************** MONOBEHAVIOUR *****************************************
	// **********************************************************************************************

	// Use this for initialization
	void Awake () {
		player = GameObject.FindGameObjectWithTag ("Player").transform;
		rangeSphereCol = GetComponent<SphereCollider> ();
		parentEnemy = GetComponentInParent<Enemy> ();

		initialBufferTimer = 0.5f;
	}
	
	// Update is called once per frame
	void Update () {

		// at the start, have some buffer
		if (initialBufferTimer > 0) {
			initialBufferTimer -= Time.deltaTime;
		}

		// update range 
		if (rangeSphereCol.radius != parentEnemy.GetRange()) {
			
			rangeSphereCol.radius = parentEnemy.GetRange();
		}
	}

	// Called ALMOST every frame a collider is within the trigger
	void OnTriggerStay (Collider other) {

		// at the start, have some buffer
		if (initialBufferTimer > 0) {
			return;
		}

		if (other.gameObject.tag == "Player") {
			if ((InFOV (player) && (IsDirectLOS (player) || isXRay)) || IsInHearingRange (player)) {
				parentEnemy.SightPlayer ();
			} else {
				parentEnemy.LostSightPlayer();
			}
		}
	}

	void OnTriggerExit (Collider other) {
		if (other.gameObject.tag == "Player") {
			parentEnemy.LostSightPlayer();
		}
	}

	// **********************************************************************************************
	// ***************************************** PRIVATES *******************************************
	// **********************************************************************************************

	/**
	 * Given a Transform, check if that transform is within the FOV
	 */
	private bool InFOV (Transform other) {
		float angle = Vector3.Angle(transform.forward, other.position - transform.position);
		return angle <= 0.5f * parentEnemy.GetFOVAngle();
	}

	/**
	 * Given a Transform, check if there is a direct LOS to that Transform
	 */
	private bool IsDirectLOS (Transform other) {
		Vector3 fwd = other.transform.position - transform.position;
		Ray ray = new Ray(transform.position + new Vector3(0f, 1f, 0f), fwd);
		RaycastHit hitResult;

		if (Physics.Raycast(ray, out hitResult, parentEnemy.GetRange()))
		{
			return (hitResult.collider.gameObject.tag == "Player");
		}
		return false;
	}

	private bool IsInHearingRange (Transform other) {
		Vector3 fwd = other.transform.position - transform.position;
		return fwd.magnitude <= 2.5f;
	}
}
