﻿using UnityEngine;
using System.Collections;

public class TeamEnemyChild : MonoBehaviour, Enemy {
	
	// INSTANCE VARIABLES
	TeamEnemyParent parent;

	private float FOVAngle;
	private float range;
	private float speed;
	private int currentPatrolPt;

	// PUBLICLY EDITABLE VARIABLES
	public Transform[] patrolPoints;

	// OBJECT COMPONENTS
	NavMeshAgent nav;

	Quaternion lookRotation;
	Vector3 lookDirection;

	// EXTERNAL OBJECTS/COMPONENTS
	GameObject player;
	PlayerHealth pHealth;

	GameObject sIconIdle;
	GameObject sIconStunned;
	GameObject sIconSearching;
	GameObject sIconAlert;

	// PRIVATES AND TIMERS
	float patrolPointTimer;
	Vector3 currentDest;
	private float speedMod;
	private float speedModTimer;
	private float rangeMod;
	private float rangeModTimer;

	// **********************************************************************************************
	// ************************************** MONOBEHAVIOUR *****************************************
	// **********************************************************************************************

	// Use this for initialization
	void Awake () {
		parent = GetComponentInParent<TeamEnemyParent>();

		player = GameObject.FindGameObjectWithTag ("Player");
		pHealth = player.GetComponent<PlayerHealth> ();
		nav = GetComponent<NavMeshAgent> ();

		FOVAngle = parent.defaultFOVAngle;
		range = parent.defaultRange;
		speed = parent.defaultSpeed;
		currentPatrolPt = 0;

		speedMod = 0;
		rangeMod = 0;
		speedModTimer = 0;
		rangeModTimer = 0;

		nav.speed = speed;

		//Enemy status icon assignment and initialization
		sIconIdle = GameObject.FindGameObjectWithTag("Icon_Idle");
		sIconStunned = GameObject.FindGameObjectWithTag("Icon_Stunned");
		sIconSearching = GameObject.FindGameObjectWithTag("Icon_Searching");
		sIconAlert = GameObject.FindGameObjectWithTag("Icon_Alert");

		sIconIdle.SetActive (false);
		sIconStunned.SetActive (false);
		sIconSearching.SetActive (false);
		sIconAlert.SetActive (false);
	}

	// Update is called once per frame
	void Update () {

		if (parent.GetState() == EnemyState.ALERT) {

			//Change status icon
			sIconIdle.SetActive(false);
			sIconStunned.SetActive (false);
			sIconSearching.SetActive (false);
			sIconAlert.SetActive (true);

			currentDest = parent.playerLKP;

			if (InFOV(player.transform) && ((player.transform.position - transform.position).magnitude <= 0.5)) {
				AttackPlayer(pHealth);
			}
		} else if (parent.GetState() == EnemyState.SEARCHING) {
			currentDest = parent.playerLKP;
		} else if (parent.GetState() == EnemyState.IDLE) 
		{
			//Change status icon
			sIconIdle.SetActive(true);
			sIconStunned.SetActive(false);
			sIconSearching.SetActive (false);
			sIconAlert.SetActive (false);

			currentDest = Patrol ();
		}

		if (speedModTimer > 0) {
			speedModTimer -= Time.deltaTime;
			speed = parent.defaultSpeed - (parent.defaultSpeed * speedMod);
			if (speedModTimer <= 0) {
				SightMe (player.transform);
				speed = parent.defaultSpeed;
				print ("in here");
			}
		}

		if (rangeModTimer > 0) {
			rangeModTimer -= Time.deltaTime;
			range = parent.defaultRange  - (range * rangeMod);
		} else {
			range = parent.defaultRange;
		}

		nav.SetDestination (currentDest);
		nav.speed = speed;
	}


	// **********************************************************************************************
	// ****************************************** ENEMY *********************************************
	// **********************************************************************************************

	/**
	 * Return the current state of the enemy
	 */
	public EnemyState GetState () {
		return parent.GetState();
	}

	/**
	 * Return the current range of the enemy
	 */
	public float GetRange () {
		return range;
	}

	/**
	 * Return the current speed of the enemy
	 */
	public float GetSpeed () {
		return speed;
	}

	/**
	 * Return the current FOV angle of the enemy
	 */
	public float GetFOVAngle () {
		return FOVAngle;
	}

	/**
	 * Given a state to change to, change the enemy's state
	 */
	public void ChangeState (EnemyState s)
	{
		parent.ChangeState (s);
	}

	/**
	 * Sight the transform
	 * Useful for alerting enemy in sonar
	 */
	public void SightMe (Transform t) {
		parent.SightMe (t);
	}

	/**
	 * Sight the player
	 * Useful for alerting enemy when in FOV
	 */
	public void SightPlayer () {
		parent.SightPlayer ();
	}

	/**
	 * Lose sight of player
	 * Useful for starting search phase, returning to idle
	 */
	public void LostSightPlayer () {
		parent.LostSightPlayer ();
	}


	/**
	 * Given a float value, multiply current speed by that value for specified time
	 **/
	public void ModifySpeed (float mod, float time)
	{
		speedMod = mod;
		speedModTimer = time;
	}

	/**
	 * Given a float value, multiply current range by that value for specified time
	 */
	public void ModifyRange (float mod, float time)
	{
		rangeMod = mod;
		rangeModTimer = time;
	}

	/**
	 * Given a float value, multiply current FOV angle by that value for specified time
	 */
	public void ModifyFOV (float mod, float time)
	{

	}


	// **********************************************************************************************
	// ***************************************** PRIVATES *******************************************
	// **********************************************************************************************

	/**
	 * Given a player GameObject to attack, begin attacking /damaging that player
	 */
	private void AttackPlayer (PlayerHealth playerHealth)
	{
		
	}


	/**
	 * If enemy is in the 'IDLE' state, this method will be used
	 */
	Vector3 Patrol()
	{
		if (patrolPoints.Length == 0) {
			return transform.position;
		}

		if (transform.position == nav.destination) 
		{
			int nextPatrolPoint = currentPatrolPt+1;

			if(nextPatrolPoint > (patrolPoints.Length-1))
			{
				nextPatrolPoint = 0;
			}


			if (patrolPointTimer <= 0.4f) 
			{
				lookDirection = ((patrolPoints[nextPatrolPoint].position-transform.position).normalized);
				lookRotation = Quaternion.LookRotation(lookDirection);
				transform.rotation = Quaternion.Slerp (transform.rotation, lookRotation, 10f*Time.deltaTime);
			}
			if (patrolPointTimer <= 0)
			{ 

				currentPatrolPt++;
				patrolPointTimer = 3f;
			}

			patrolPointTimer -= Time.deltaTime;
		}

		if (currentPatrolPt > patrolPoints.Length - 1) 
		{
			currentPatrolPt = 0;
		}

		//Change status icon
		sIconIdle.SetActive(true);

		return patrolPoints [currentPatrolPt].position;
	}

	/**
	 * Given a Transform, check if that transform is within the FOV
	 */
	private bool InFOV (Transform other) {
		float angle = Vector3.Angle(transform.forward, other.position - transform.position);
		return angle <= 0.5f * FOVAngle;
	}
}
