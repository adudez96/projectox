﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour 
{
	//CLASS INSTANCES

	//PUBLIC VARIABLES
	public int startingHealth;
	public bool isDead;

	public bool isDamaged;

	//GAMEOBJECTS AND COMPONENTS
	public Text healthText;

	//PRIVATE VARIABLES

	private int currentHealth;

	/**
	* Method to handle initialization of variables 
	 */
	void Awake()
	{
		currentHealth = startingHealth;
		isDamaged = false;
		isDead = false;

	}

	void Start () 
	{
	
	}
	
	/**
	* Method that updates game events 
	 **/
	public void Update () 
	{
		CheckDamage ();
	}

	/**
	* Check if player has taken damage 
	 **/
	void CheckDamage()
	{
		if (isDamaged) 
		{
			//GUI Flashes Red
		} 

		else 
		{
			//Interpolate back to clear screen
		}

		//Reset 'isDamaged' flag back to false
		isDamaged = false;
	}

	/**
	* Method to handle when player takes damage 
	 **/
	public void TakeDamage(int damageAmount)
	{
		isDamaged = true;

		currentHealth -= damageAmount;

		healthText.text = ""+ currentHealth;

		if (currentHealth <= 0 && !isDead)
		{
			PlayerDie ();
		}
	}

	/**
	* Method to handle player death 
	 **/
	void PlayerDie()
	{
		isDead = true;
	}
}
