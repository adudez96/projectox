﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour 
{

	// CLASS INSTANCES 

	// PUBLIC VARIABLES

	//GAMEOBJECTS AND COMPONENTS 
	GameObject player;

	Camera mainCamera;

	Transform target;

	//PRIVATE/LOCAL VARIABLES
	float smoothing = 10f;
	Vector3 offset;

	// Method used for initializing variables prior to script run-time, once the game has loaded
	void Awake () 
	{
		player = GameObject.FindGameObjectWithTag ("Player");
		target = player.transform;
		offset = transform.position - target.position;	
	}
	
	// FixedUpdate is called at regular intervals and used for physics and time-based updates
	void FixedUpdate () 
	{
		Vector3 targetCamPos = target.position + offset;
		transform.position = Vector3.Lerp(transform.position,targetCamPos,smoothing * Time.deltaTime);
	}
}
