﻿using UnityEngine;
using System.Collections;

public class ObjectUser : MonoBehaviour {

	public GameObject sonarPrefab;
	public GameObject empPrefab;
	public GameObject stunPrefab; 

	public int[] inventory;

	public float maxThrowSpeed = 20;

	bool isPressing;
	float throwTimer;

	// Use this for initialization
	void Awake() {
		inventory = new int[3];
		inventory [0] = 0; 	// SONAR - distraction
		inventory [1] = 0; 	// EMP - blind
		inventory [2] = 0; 	// STUN - freeze

		isPressing = false;
		throwTimer = 0;
	}
	
	// Update is called once per frame
	void Update () {

		if (isPressing) {
			throwTimer += Time.deltaTime;
			if (throwTimer > 2) {
				throwTimer = 2;
			}
		}

		if (Input.GetKeyDown (KeyCode.Alpha1) || Input.GetKeyDown (KeyCode.Alpha2) || Input.GetKeyDown (KeyCode.Alpha3)) {
			isPressing = true;
		}

		if (isPressing) {
			if (Input.GetKeyUp (KeyCode.Alpha1) && inventory [0] == 1) {
				GameObject item = Instantiate (sonarPrefab, transform.position + transform.rotation * new Vector3 (0, 1, 1), transform.rotation) as GameObject;
				item.GetComponent<Rigidbody> ().velocity = transform.forward * maxThrowSpeed * throwTimer/2;
				inventory [0] = 0;
				isPressing = false;
			} else if (Input.GetKeyUp (KeyCode.Alpha2) && inventory [1] == 1) {
				GameObject item = Instantiate (empPrefab, transform.position + transform.rotation * new Vector3 (0, 1, 1), transform.rotation) as GameObject;
				item.GetComponent<Rigidbody> ().velocity = transform.forward * maxThrowSpeed * throwTimer/2;
				inventory [1] = 0;
				isPressing = false;
			} else if (Input.GetKeyUp (KeyCode.Alpha3) && inventory [2] == 1) {
				GameObject item = Instantiate (stunPrefab, transform.position + transform.rotation * new Vector3 (0, 1, 1), transform.rotation) as GameObject;
				item.GetComponent<Rigidbody> ().velocity = transform.forward * maxThrowSpeed * throwTimer/2;
				inventory [2] = 0;
				isPressing = false;
			}
		}

		print ("INVENTORY: " + inventory[0] + inventory[1] + inventory[2]);
	}

	public bool Collect (int type) {
		// type 1 ==> sonar
		// type 2 ==> EMP
		// type 3 ==> other

		print ("thing" + type + "   " + inventory [type]);

		if (inventory [0] + inventory [1] + inventory [2] >= 2 || inventory [type] == 1) {
			return false;
		} else {
			inventory [type] = 1;
			return true;
		}
	}
}
