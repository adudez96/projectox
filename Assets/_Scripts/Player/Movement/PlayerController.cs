﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour 
{

	//SUP HOMIEEEEEEEE

	// CLASS INSTANCES

	// PUBLIC VARIABLES


	// GAMEOBJECTS AND COMPONENTS

	Rigidbody rb;
	Transform playerTransform;
	LayerMask groundMask;

	// PRIVATE VARIABLES

	[SerializeField]
	private float speed;

	Vector3 movement;
	Vector3 lookPosition;


	/**
	* Method used for initializing variables prior to script run-time, once the game has loaded
	*/

	void Awake () 
	{
		rb = GetComponent<Rigidbody> ();
		playerTransform = GetComponent<Transform> ();
		groundMask = LayerMask.GetMask ("Ground");
	}

	/**
	* FixedUpdate is called at regular intervals. Primarily used for physics and other time-based updates;
	*/

	void FixedUpdate () 
	{
		float moveHorizontal = Input.GetAxisRaw ("Horizontal");
		float moveVertical = Input.GetAxisRaw ("Vertical");

		Move (moveHorizontal, moveVertical);
		Look ();
	}

	/**
	* Method to define the movement of the player. The player is moved by altering the position of the player object's Rigidbody component
	*/
	void Move(float h, float v)
	{
		movement.Set (h, 0f, v);
		movement = (movement.normalized)*speed*Time.deltaTime;

		rb.MovePosition (playerTransform.position + movement);
	}

	/**
	* Method to define where the player is looking. A raycast will point to the mouse position, and the player will look towards the mouse position so long as the raycast hits the "Ground" layer mask
	*/
	void Look()
	{
		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
		RaycastHit hit;

		if (Physics.Raycast (ray, out hit, 100, groundMask))
		{
			lookPosition = hit.point;
		}

		Vector3 lookDirection = lookPosition - transform.position;
		lookDirection.y = 0;

		transform.LookAt (transform.position + lookDirection, Vector3.up);
	}
}
